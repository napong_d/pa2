package ku.util;

import java.util.EmptyStackException;

/**
 * stack
 * @author Napong Dungduangsasitorn
 * 
 * @param <T> is input.
 */

public class Stack<T> {

	private T[] items; // items on the stack
	private int index = 0;
	/**
	 * constructor that specifies the capacity of the stack.
	 * @param capacity
	 */

	public Stack(int capacity){
		items = (T[]) new Object[capacity];
	}

	/**
	 * the maximum number of elements that this Stack can hold.
	 * @return -1 if unknown or infinite.
	 */
	public int capacity(){
		if(items.length >= 0){
			return items.length;
		}
		return -1;
	}

	/**
	 * check stack is empty or not.
	 * @return true if stack is empty.
	 */
	public boolean isEmpty(){
		if(index != 0){
			return false;
		}
		return true;
	}

	/**
	 * check stack is full or not.
	 * @return true if stack is full.
	 */
	public boolean isFull(){
		if(index != items.length){
			return false;
		}

		return true;
	}


	/**
	 * show item on top of stack but not remove item.
	 * @return the item on the top of the stack, without removing it. 
	 * If the stack is empty, return null.
	 */
	public T peek(){
		if(isEmpty())
			return null;
		return items[index-1];
	}

	/**
	 * show item on top of stack and remove it.
	 * @return the item on the top of the stack,and remove it from the stack.
	 * Throws: EmptyStackException if stack is empty.
	 */
	public T pop(){
		if(isEmpty()){
			throw new EmptyStackException();
		}
		T popitem = items[index-1];
		items[index-1] = null;
		index--;
		
		return popitem;
	}

	/**
	 * push a new item onto the top of the stack. 
	 * If the stack is already full, this method does nothing... 
	 * its the programmer's responsibility to check isFull()
	 * before trying to push something onto stack.
	 * The parameter (obj) must not be null.
	 * Throws: InvalidArgumentException if parameter is null.
	 * @param obj
	 */
	public void push (T obj){
		if(obj == null){
			throw new IllegalArgumentException();
		}
		if(!isFull()){
			this.items[size()] = obj;
			this.index++;
		}
	}

	/**
	 * size of stack.
	 * @return the number of items in the stack. 
	 * Returns 0 if the stack is empty
	 */
	public int size(){
		if(isEmpty())
			return 0;
		return index;
	}
}
