package ku.util;

import java.util.Iterator;
import java.util.NoSuchElementException;

/**
 * 
 * @author Napong Duangduangsasitorn
 *
 */
public class ArrayIterator<T> implements Iterator<T>{

	private T[] array;
	private int cursor;
	
	public ArrayIterator(T[] array){
		this.array = array;
		cursor = 0;
	}
	/**
	 * Return the next non-null element in the array.
	 * If there are no more elements, throws NoSuchElementException.
	 *@return 
	 */
	public T next(){
		if(hasNext()){
			for(int i = this.cursor; i < array.length;i++){
				cursor++;
				if(this.array[i] != null){
					return this.array[i];
				}
			}
		}
		throw new NoSuchElementException( );
	}
	
	/**
	 * Returns true if next() can return another non-null array element.
	 * @return false if no more elements.
	 */
	public boolean hasNext(){
		for(int i = this.cursor;i < array.length;i++){
			if(array[i] != null){
				return true;
			}
		}
		return false;
	}
	/**
	 * remove element
	 */
	public void remove(){
		array[cursor-1] = null;
	}
}
